package com.liannetsys.otc.partner;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.crypto.digests.MD5Digest;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;

import com.fasterxml.jackson.databind.JsonNode;

public class SignatureUtil {
	
	private static PublicKey publicKey;
	private static PrivateKey privateKey;
	private static String privateKeyFile="private-key.pem";
	private static String publicKeyFile="public-key.pem";

	//从 pem 档里读取RSA私钥
	public static PrivateKey loadPrivateKeyFromPemFile(String keyFileName)
			throws IOException, InvalidKeySpecException, NoSuchAlgorithmException {		
		try (InputStream is = SignatureUtil.class.getClassLoader().getResourceAsStream(keyFileName)) {		
			try (InputStreamReader fr = new InputStreamReader(is); PEMParser parser = new PEMParser(fr)) {
				Object obj = parser.readObject();
				JcaPEMKeyConverter converter = new JcaPEMKeyConverter();
				if (obj instanceof PrivateKeyInfo) {
					return converter.getPrivateKey((PrivateKeyInfo) obj);
				} else if (obj instanceof PEMKeyPair) {
					PrivateKeyInfo privateKeyInfo = ((PEMKeyPair) obj).getPrivateKeyInfo();
					if (privateKeyInfo != null) {
						return converter.getPrivateKey(privateKeyInfo);
					}
				}
			}
		}
		throw new InvalidKeySpecException("Invalid Private key.");
	}
	
	//从 pem 档里读取RSA公钥
	public static PublicKey loadPublicKeyFromPemFile(String keyFileName)
			throws FileNotFoundException, IOException, InvalidKeySpecException {
		// Loads a public key from the specified key file name
		try (InputStream is = SignatureUtil.class.getClassLoader().getResourceAsStream(keyFileName)) {			
			try (InputStreamReader fr = new InputStreamReader(is); PEMParser parser = new PEMParser(fr)) {								
				JcaPEMKeyConverter converter = new JcaPEMKeyConverter();				
				Object obj = parser.readObject();				
				if (obj instanceof SubjectPublicKeyInfo) {					
					return converter.getPublicKey((SubjectPublicKeyInfo) obj);
				} else if (obj instanceof PEMKeyPair) {
					SubjectPublicKeyInfo publicKeyInfo = ((PEMKeyPair) obj).getPublicKeyInfo();
					if (publicKeyInfo != null) {
						return converter.getPublicKey(publicKeyInfo);
					}
				}
			}
		}
		throw new InvalidKeySpecException("Invalid Public key .");
	}

	public static PrivateKey getPrivateKey() throws InvalidKeySpecException, NoSuchAlgorithmException, IOException {
		if (privateKey == null) {
			privateKey = loadPrivateKeyFromPemFile(privateKeyFile);
		}
		return privateKey;
	}

	public static PublicKey getPublicKey() throws InvalidKeySpecException, NoSuchAlgorithmException, IOException {
		if (publicKey == null) {
			publicKey = loadPublicKeyFromPemFile(publicKeyFile);																						
		}
		return publicKey;
	}

	//用私钥为字串生成并返回签署
	public static String generateSignature(String rawstr) throws InvalidKeySpecException, NoSuchAlgorithmException,
			IOException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		MD5Digest md5 = new MD5Digest();
		//支持非英文字符
		byte[] bytes= rawstr.getBytes("utf-8");
		//生成md5
		md5.update(bytes,0, bytes.length);
		byte[] digest = new byte[md5.getDigestSize()];
		md5.doFinal(digest, 0);	
		//转为小写hex字串的
		String hash = Hex.encodeHexString(digest);
		//RSA加密
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.ENCRYPT_MODE, getPrivateKey());
		//BASE64编码并返回
		return Base64.encodeBase64String(cipher.doFinal(hash.getBytes()));
	}

	

	//把一个Map里面的参数值根据参数名称的顺序排列串在一起，生成请求的签名值
	public static String generateValueString(Map<String, String> params) {
		//获取参数名
		List<String> fields = new ArrayList<>();
		for (String key : params.keySet()) {
			fields.add(key);
		}
		//根据名字排列
		fields.sort(new Comparator<String>() {
			@Override
			public int compare(String s1, String s2) {
				return s1.compareTo(s2);
			}
		});
		//根据排列好的参数名，把他们的值串在一起生成一个字串
		StringBuilder sb = new StringBuilder();
		for (String key : fields) {
			sb.append(params.get(key));
		}
		return sb.toString();
	}
	
	//为map里的参数生成签署
	public static String generateSignature(Map<String, String> params) throws InvalidKeyException, InvalidKeySpecException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException {
		String value = generateValueString(params);
		//用这个字串生成签署
		return generateSignature(value);
	}

	//为map里的参数验证签署
	public static boolean validateSignature(Map<String,String> params,String signature) throws InvalidKeyException, FileNotFoundException, InvalidKeySpecException, NoSuchAlgorithmException, NoSuchPaddingException, IOException{
		String value=generateValueString(params);
		return validateSignature(value,signature ); 
	}
	
	
	//签署验证
	public static boolean validateSignature(String data, String signature)
			throws FileNotFoundException, InvalidKeySpecException, IOException, NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidKeyException {
		
		//根据签名方式，生成md5的值
		MD5Digest md5 = new MD5Digest();
		byte[] bytes = data.getBytes("UTF-8");
		md5.update(bytes, 0, bytes.length);		
		byte[] digest = new byte[md5.getDigestSize()];
		md5.doFinal(digest, 0);
		String hash = Hex.encodeHexString(digest);
		System.out.println("expected signature:" + hash);
		//利用公钥进行解密
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.DECRYPT_MODE, getPublicKey());
		byte[] rawSignature = Base64.decodeBase64(signature);
		try {
			byte[] decrypted = cipher.doFinal(rawSignature);
			String sign = new String(decrypted, "UTF-8");
			System.out.println("extracted signature:" + sign);
			//比较解密的值跟生成的MD5
			return hash.equals(sign);
		} catch (Exception e) {			
			e.printStackTrace();
			return false;
		}
	}
	
	public static final boolean validateSignature(JsonNode node) throws InvalidKeyException, FileNotFoundException,
			InvalidKeySpecException, NoSuchAlgorithmException, NoSuchPaddingException, IOException {
		StringBuilder sb = new StringBuilder();
		generateKeyString(node, sb);
		return validateSignature(sb.toString(), node.get("sign").asText());
	}

	public static void generateKeyString(JsonNode node, StringBuilder sb) {
		if (node.isValueNode()) {
			sb.append(node.asText());
		} else {
			List<String> keys = new ArrayList<>();
			for (Iterator<String> it = node.fieldNames(); it.hasNext();) {
				keys.add(it.next());
			}
			keys.sort(new Comparator<String>() {
				@Override
				public int compare(String o1, String o2) {
					return o1.compareTo(o2);
				}
			});
			for (String key : keys) {
				JsonNode field = node.get(key);
				if (!"sign".equals(key)) {
					generateKeyString(field, sb);
				}
			}
		}
	}

	
}
