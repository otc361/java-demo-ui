<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<% java.util.Map<String,String[]> params = request.getParameterMap();
   java.util.Map<String,String> paramMap = new java.util.HashMap<String,String>();
   for (java.util.Map.Entry<String,String[]> entry : params.entrySet()){
  	 String[] value=  entry.getValue();
  	 if (value.length>0){
  	   paramMap.put(entry.getKey(), value[0]);
  	 } else {
  	   paramMap.put(entry.getKey(), "");
  	 }  	 
   }
%>   
<%=com.liannetsys.otc.partner.SignatureUtil.generateSignature(paramMap)%>