<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>OTC Payment Demo</title>
<style>
   input {
     display:block;
     width: 400px;
   }
</style>
</head>
<body>
	<form id="frm" method="post"
		action="http://demo.synerteqsys.com:78/api/create_sell_order_ui" onsubmit="addSignature">
		<input hidden name="kyc" value="1"> 
		<input name="idCardType" placeholder="idCardType" value="ID"> 
		<input name="idCardNum" value="na" placeholder="idCardNum"> 
	    <input name="username" value="test user" placeHoder="username"> 
		<input name="areaCode" value="+86" placeHolder="areaCode"> 
		<input name="phoneNum" value="na" placeHolder="phoneNum"> 
		<input name="email" value="test@testing.com" placeHolder="email"> 
	    <input type="hidden" name="coin" value="CZDT"> 
		<input name="referenceNo" id="ref" value="" placeHolder="referenceNo"> 
	    <input name="amount" value="1000" placeHolder="amount">
	    <input name="bankAccount" value="12345678" placeHolder="bankAccount">
	    <input name="bankName" value="建设银行" ankName">
	    <input name="bankBranch" value="总行" placeHolder="bankBranch"> 
		<input name="totalPrice" value="980" placeHolder="totalPrice"> 
		<input name="orderTime" value="" id="orderTime" placeHolder="orderTime"> 
		<input name="preferredPayment" value="ALIPAY" placeHolder="preferredPayment"> 
		<input name="ts" id="ts" value="" placeHolder="ts"> 
		<input name="apiKey" value="54ebbbf7a808b01896a1844e463ebf29" placeHolder="apiKey"> 
		<input name="sign" id="sign" value="" placeHolder="sign">
		<input name="successUrl" value="http://localhost:8080/otcdemo/success.jsp" placeHolder="successUrl">			
		<input name="failUrl" value="http://localhost:8080/otcdemo/fail.jsp" placeHolder="failUrl">
		<input name="cancelUrl" value="http://localhost:8080/otcdemo/cancel.jsp" placeHolder="cancelUrl">
		<input name="datafeedUrl" value="http://localhost:8080/otcdemo/datafeed.jsp" placeHolder="datafeedUrl">
		<input type="button" value="populate" id="btnFill" name="fill">
		
		<input type="submit" id="sub" disabled>
	</form>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script>  
	   $(function(){
	  	 $("#btnFill").click(function(e){
	  		 e.preventDefault(); 
	  		 var ts=new Date().getTime();
	  		 $("#ts, #orderTime, #ref").val(ts);
	  	   $.ajax({
           	type: "get",
           url: "sign.jsp",
           	data: $("#frm").serialize(), // serializes the form's elements.
           	success: function(data)
           	{
            	 $("#sign").val(data.trim());
            	 $("#sub").removeAttr("disabled");
           	}
         	});
	  	 });
	   })
	  
	</script>	
</body>
</html>