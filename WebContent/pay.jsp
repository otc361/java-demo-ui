<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>OTC Payment Demo</title>
<style>
   input {
     display:block;
     width: 400px;
   }
</style>
</head>
<body>
	<form id="frm" method="post"
		action="https://www.otc361.com/api/create_buy_order_ui">
		<input hidden name="kyc" value="1"> 
	    <input name="username" value="test user" placeHoder="username"> 
	    <input type="hidden" name="coin" value="USDT"> 
		<input name="referenceNo" id="ref" value="" placeHolder="referenceNo"> 
	    <input name="amount" value="1000" placeHolder="amount"> 
		<input name="orderTime" value="" id="orderTime" placeHolder="orderTime"> 
		<input name="preferredPayment" value="BANK" placeHolder="preferredPayment"> 
		<input name="ts" id="ts" value="" placeHolder="ts"> 
		<input name="apiKey" value="1bba303d384f63ded636edf22fae29f4" placeHolder="apiKey"> 
		<input name="sign" id="sign" value="" placeHolder="sign">
		<input name="successUrl" value="http://localhost/success.php" placeHolder="successUrl">			
		<input name="failUrl" value="http://localhost/fail.php" placeHolder="failUrl">
		<input name="cancelUrl" value="http://localhost/cancel.php" placeHolder="cancelUrl">
		<input name="datafeedUrl" value="http://localhost/datafeed.jsp" placeHolder="datafeedUrl">
		<input type="button" value="populate" id="btnFill" name="fill">
		
		<input type="submit" id="sub" disabled>
	</form>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script>  
	   $(function(){
	  	 $("#btnFill").click(function(e){
	  		 e.preventDefault(); 
	  		 var ts=new Date().getTime();
	  		 $("#ts, #orderTime, #ref").val(ts);
	  	   $.ajax({
           	type: "get",
           url: "sign.jsp",
           	data: $("#frm").serialize(), // serializes the form's elements.
           	success: function(data)
           	{
            	 $("#sign").val(data.trim());
            	 $("#sub").removeAttr("disabled");
           	}
         	});
	  	 });
	   })
	  
	</script>	
</body>
</html>